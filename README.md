It works one line at a time doing these things:

 * Make sure there's 5 tab separated columns, add a blank one if there's one missing. The columns seem to be something like: CODE, TITLE, PRICE, DETAILS, WRITERS
 * Ensure there's a set of quotes around everything, including the blank values
   * EXCLUDING short column values that contain a period (to find prices) or that are just 0 (though I have a feeling that if the prices were quoted this would also be fine)

Then it dumps that into a file with ".fixed" added to it like you've seen!
